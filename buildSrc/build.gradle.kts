/******************************************************************************
MIT License

Copyright (c) 1999-2021 A Bit of Help, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 ******************************************************************************/
@file:Suppress("UNUSED_VARIABLE", "UnstableApiUsage")

/********************************************************************************
 *         I  M  P  O  R  T  E  D     D  E  P  E  N  D  E  N  C  I  E  S        *
 ********************************************************************************/
/********************************************************************************
 *                    G  R  A  D  L  E     P  L  U  G  I  N  S                  *
 ********************************************************************************/
plugins {
    `kotlin-dsl`
}
/********************************************************************************
 *                    B  U  I  L  D     S  E  T  T  I  N  G  S                  *
 ********************************************************************************/
/********************************************************************************
 *            B  U  I  L  D      C  O  N  F  I  G  U  R  A  T  I  O  N          *
 ********************************************************************************/
/********************************************************************************
 *              M  A  V  E  N     R  E  P  O  S  I  T  O  R  I  E  S            *
 ********************************************************************************/
repositories {  mavenCentral(); gradlePluginPortal() }
/********************************************************************************
 *                       D  E  P  E  N  D  E  N  C  I  E  S                     *
 ********************************************************************************/
dependencies {
    // Adding these plugin packages to the classpath, so the
    // jvmplugin can use them in the plugin block.
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin")
    implementation("com.gradle.publish:plugin-publish-plugin:0.15.0")
}
/********************************************************************************
 *                       U  N  I  T     T  E  S  T  I  N  G                     *
 ********************************************************************************/
/********************************************************************************
 *           I  N  T  E  G  R  A  T  I  O  N      T  E  S  T  I  N  G           *
 ********************************************************************************/
/********************************************************************************
 *                       G  R  A  D  L  E     T  A  S  K  S                     *
 ********************************************************************************/
/********************************************************************************
 *                 M  A  V  E  N     P  U  B  L  I  S  H  I  N  G               *
 ********************************************************************************/
/********************************************************************************
 *               D  O  C  K  E  R     P  U  B  L  I  S  H  I  N  G              *
 ********************************************************************************/
/********************************************************************************
 *                             H  E  L  P  E  R  S                              *
 ********************************************************************************/
