/******************************************************************************
MIT License

Copyright (c) 1999-2021 A Bit of Help, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 ******************************************************************************/
@file:Suppress("SimpleRedundantLet", "UNUSED_VARIABLE", "UnstableApiUsage")

package com.abitofhelp.gradle.helpers

/********************************************************************************
 *         I  M  P  O  R  T  E  D     D  E  P  E  N  D  E  N  C  I  E  S        *
 ********************************************************************************/
import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalog
import org.gradle.api.artifacts.VersionCatalogsExtension
import org.gradle.api.initialization.Settings
import java.text.SimpleDateFormat
import java.util.*

/*******************************************************************************
 *                    P  U  B  L  I  C     M  E  T  H  O  D  S                 *
 *******************************************************************************/

fun sayHello() {
    println("howdy")
}



fun copyright(
    clientOrg: String,
    firstYear: String = "2000"
): String = "© $firstYear-${
    SimpleDateFormat("yyyy").format(Date())
} ${clientOrg}, All Rights Reserved."

fun buildAttributeMap(
    archiveFileName: String,
    archiveVersion: String,
    project: Project,
    pomName: String,
    pomClientOrg: String,
    pomDeveloper: String,
    pomDeveloperOrgUri: String,
    pomCopyrightStartedYear: String,
    mainClass: String? = null
): Map<String, String> {

    val implementationTitle = when {
        archiveFileName.endsWith("-sources.jar") -> "Implementation-Title" to "Source Code for the '$pomName' Project."
        archiveFileName.endsWith("-htmldoc.jar") -> "Implementation-Title" to "HTML Documentation for the '$pomName' Project."
        archiveFileName.endsWith("-javadoc.jar") -> "Implementation-Title" to "Javadoc Documentation for the '$pomName' Project."
        else                                     -> "Implementation-Title" to "Implementation of the '$pomName' Project."
    }

    val attributes = mutableMapOf<String, String>(
        "Build-Engine" to "Gradle ${project.gradle.gradleVersion}",
        "Build-Jdk" to "${System.getProperty("java.version")} (${System.getProperty("java.vendor")} ${
            System.getProperty("java.vm.version")
        })",
        "Build-OS" to "${System.getProperty("os.name")} ${System.getProperty("os.arch")} ${
            System.getProperty(
                "os.version"
            )
        }",
        "Build-Revision" to archiveVersion,
        "Build-Timestamp" to Date().toInstant().toString(),
        "Built-By" to System.getProperty("user.name"),
        "Class-Path" to project.configurations.getByName("runtimeClasspath").asPath,
        "Copyright" to copyright(pomClientOrg, pomCopyrightStartedYear),
        "Created-By" to pomDeveloper,
        "Created-By-Uri" to pomDeveloperOrgUri,
        implementationTitle,
        "Manifest-Version" to "1.0",
        "Name" to archiveFileName
    )

    if (mainClass != null) {
        attributes["Main-Class"] = mainClass
        attributes.toSortedMap()
    }
    return attributes.toMap()
}

fun base64Decode(encoded: String?): String {
    return String(Base64.getDecoder().decode(encoded.toString())).trim()
}

fun getGradlePropertyValue(
    settings: Settings,
    key: String
): String {
    return settings.javaClass.getMethod("getProperty", String::class.java).invoke(settings, key).toString()
}

val Project.libs: VersionCatalog
    get() = extensions.getByType(VersionCatalogsExtension::class.java).named("libs")

/*******************************************************************************
 *                E  M  B  E  D  D  E  D     M  E  T  H  O  D  S               *
 *******************************************************************************/
/*******************************************************************************
 *    C  O  M  P  A  N  I  O  N     O  B  J  E  C  T    M  E  T  H  O  D  S    *
 *******************************************************************************/
/*******************************************************************************
 *                 P  R  I  V  A  T  E     M  E  T  H  O  D  S                 *
 *******************************************************************************/

