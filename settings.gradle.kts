/******************************************************************************
MIT License

Copyright (c) 1999-2021 A Bit of Help, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 ******************************************************************************/
@file:Suppress("SimpleRedundantLet", "UNUSED_VARIABLE", "UnstableApiUsage")

enableFeaturePreview("VERSION_CATALOGS")
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

/********************************************************************************
 *                   G  L  O  B  A  L    P  L  U  G  I  N  S                    *
 ********************************************************************************/
pluginManagement {
    plugins {
        id("org.jetbrains.kotlin.plugin.allopen") version settings.extra["kotlin"] as String?
        id("com.gorylenko.gradle-git-properties") version settings.extra["gradleGitProperties"] as String?
        id("io.gitlab.arturbosch.detekt") version settings.extra["detekt.gradle"] as String?
        id("org.jetbrains.dokka") version settings.extra["dokka.gradle"] as String?
        id("org.jetbrains.kotlin.jvm") version settings.extra["kotlin"] as String?
        id("org.jetbrains.kotlin.kapt") version settings.extra["kotlin"] as String?
        // Required to authenticate with Azure Devop's Maven Artifact Repository.
        id("net.linguica.maven-settings") version "0.5"
        id("plugin-publish-plugin") version settings.extra["gradlePublishVersion"] as String?

        // TODO: There is a bug in using the following extension, here.
        //        extensions.findByType(VersionCatalogsExtension::class.java)?.let { versionCatalogsExtension ->
        //            versionCatalogsExtension.named("libs").apply {
        //                alias(findPlugin("kotlin.jvm.gradle").get())
        //            }
        //        }?: throw kotlin.NullPointerException("VersionCatalogsExtension::class.java does not exist")
    }

    // Set the plugin repositories for all projects.
    repositories {
        maven {
            name = "MyMaven"
            url = uri("/Users/mike/MYMAVEN")
        }
        gradlePluginPortal()
        mavenCentral()
    }
}

/********************************************************************************
 *            G  L  O  B  A  L    R  E  P  O  S  I  T  O  R  I  E  S            *
 ********************************************************************************/
dependencyResolutionManagement {
    // Enabling the use of a Gradle Version Catalog.
    defaultLibrariesExtensionName.set("deps")

    // Importing the version catalog, so it can be used by the convention plugins.
    versionCatalogs {
        create("libs") {
            // Be sure to change the version in the buildSrc settings.gradle.kts, too.
            //from(files("gradle/libs.versions.toml.bak"))
            from("com.ingios.csc.gradle:versions-catalog:3.1.0.100")

            // Only permit repositories in the settings.gradle.kts file.
            //repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)

            repositories {
                maven {
                    name = "MyMaven"
                    url = uri("/Users/mike/MYMAVEN")
                }
                mavenCentral()
                gradlePluginPortal()
            }
        }
    }

    repositories {
        repositories {
            maven {
                name = "MyMaven"
                url = uri("/Users/mike/MYMAVEN")
            }
            mavenCentral()
            gradlePluginPortal()
        }
    }
}

/********************************************************************************
 *                C  O  M  P  O  S  I  T  E    B  U  I  L  D  S                 *
 ********************************************************************************/
/********************************************************************************
 *                S  U  B  P  R  O  J  E  C  T    B  U  I  L  D                 *
 ********************************************************************************/
rootProject.name = "jvmplugin-standalone"
include("jvmplugin")
