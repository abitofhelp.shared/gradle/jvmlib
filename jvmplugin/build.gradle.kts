/******************************************************************************
MIT License

Copyright (c) 1999-2021 A Bit of Help, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 ******************************************************************************/
@file:Suppress("UNUSED_VARIABLE", "UnstableApiUsage")

/********************************************************************************
 *         I  M  P  O  R  T  E  D     D  E  P  E  N  D  E  N  C  I  E  S        *
 ********************************************************************************/
import com.abitofhelp.gradle.helpers.base64Decode
/********************************************************************************
 *                    G  R  A  D  L  E     P  L  U  G  I  N  S                  *
 ********************************************************************************/
plugins {
    `java-gradle-plugin`
    `maven-publish`
    id("com.gradle.plugin-publish")
    id("org.jetbrains.kotlin.jvm")
    signing
}
/********************************************************************************
 *                    B  U  I  L  D     S  E  T  T  I  N  G  S                  *
 ********************************************************************************/
/********************************************************************************
 *            B  U  I  L  D      C  O  N  F  I  G  U  R  A  T  I  O  N          *
 ********************************************************************************/
gradlePlugin {
    val jvmplugin by plugins.creating {
        id = "com.abitofhelp.gradle.plugins.jvmplugin"
        implementationClass = "com.abitofhelp.gradle.plugins.jvmplugin.JvmPlugin"
        displayName = "JvmPlugin"
        description =
            "This repository contains a custom Gradle plugin that configures our JVM applications for Kotlin.  It provides a standard baseline on which we add layers, such as Spring Boot."
    }
}

pluginBundle {
    website = "https://gitlab.com/abitofhelp.shared/gradle/jvmlib"
    vcsUrl = "https://gitlab.com/abitofhelp.shared/gradle/jvmlib.git"
    tags = listOf("gradle", "plugin", "jvm", "kotlin", "kapt")
}

configureFunctionTest()

configurePluginPublishing()

/********************************************************************************
 *              M  A  V  E  N     R  E  P  O  S  I  T  O  R  I  E  S            *
 ********************************************************************************/
/********************************************************************************
 *                       D  E  P  E  N  D  E  N  C  I  E  S                     *
 ********************************************************************************/
dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}
/********************************************************************************
 *                       U  N  I  T     T  E  S  T  I  N  G                     *
 ********************************************************************************/
dependencies {
    // BOMs
    testImplementation(platform(libs.findDependency("junit.bom").get()))
    testImplementation(platform(libs.findDependency("strikt.bom").get()))

    // ARTIFACTs
    testImplementation(libs.findDependency("mockk").get())
    testImplementation("io.strikt:strikt-arrow")
    testImplementation("io.strikt:strikt-jackson")
    testImplementation("io.strikt:strikt-jvm")
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter")
}
/********************************************************************************
 *           I  N  T  E  G  R  A  T  I  O  N      T  E  S  T  I  N  G           *
 ********************************************************************************/
/********************************************************************************
 *                       G  R  A  D  L  E     T  A  S  K  S                     *
 ********************************************************************************/
tasks.compileKotlin {
    kotlinOptions {
        @Suppress("SpellCheckingInspection")
        freeCompilerArgs = listOf("-Xjsr305=strict")
        allWarningsAsErrors = true
        jvmTarget = "11"
        languageVersion = "1.5"
        apiVersion = "1.5"
    }
}

tasks.compileTestKotlin {
    kotlinOptions {
        @Suppress("SpellCheckingInspection")
        freeCompilerArgs = listOf("-Xjsr305=strict")
        allWarningsAsErrors = true
        jvmTarget = "11"
        languageVersion = "1.5"
        apiVersion = "1.5"
    }
}

tasks.withType(
    Wrapper::
    class.java
)
{     // Keeps Gradle at the current release.
    gradleVersion = libs.findVersion("gradle_version").get().toString()
    description = "Generates gradlew[.bat] scripts"
    distributionType = Wrapper.DistributionType.ALL
}

tasks.withType<Test>().configureEach {
    // This task will be applied to any configurations
    // that extendFrom testImplementation or testRuntimeOnly.
    // i.e. functionalTestImplementation, integrationTestRuntimeOnly, etc.

    // Enable JUnit Platform (a.k.a. JUnit 5) support.
    useJUnitPlatform()

    testLogging {
        // ahow standard out and standard error of the test JVM(s) on the console.
        showStandardStreams = true
        events("passed", "skipped", "failed")
        exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }

    // Fail the 'test' task on the first test failure.
    failFast = true
}
/********************************************************************************
 *                 M  A  V  E  N     P  U  B  L  I  S  H  I  N  G               *
 ********************************************************************************/
/********************************************************************************
 *               D  O  C  K  E  R     P  U  B  L  I  S  H  I  N  G              *
 ********************************************************************************/
/********************************************************************************
 *                             H  E  L  P  E  R  S                              *
 ********************************************************************************/
fun configureFunctionTest() {
    // Add a source set for the functional test suite
    val functionalTestSourceSet = sourceSets.create("functionalTest") {
    }

    gradlePlugin.testSourceSets(functionalTestSourceSet)
    configurations["functionalTestImplementation"].extendsFrom(configurations["testImplementation"])

    // Add a task to run the functional tests
    val functionalTest by tasks.registering(Test::class) {
        testClassesDirs = functionalTestSourceSet.output.classesDirs
        classpath = functionalTestSourceSet.runtimeClasspath
    }

    tasks.check {
        // Run the functional tests as part of `check`
        dependsOn(functionalTest)
    }
}

fun configurePluginPublishing() {
    publishing {
        repositories {
            maven {
                name = "MyMaven"
                url = uri("/Users/mike/MYMAVEN")
            }
        }

        publications {
            create<MavenPublication>(project.extra["pom.name"].toString()) {
                from(components["java"])
                pom {
                    packaging = "jar"

                    name.set(project.extra["pom.name"].toString())
                    description.set(project.extra["pom.description"].toString())
                    url.set(project.extra["pom.url"].toString())

                    scm {
                        connection.set(project.extra["pom.scm.connection"].toString())
                        developerConnection.set(project.extra["pom.scm.developerConnection"].toString())
                        url.set(project.extra["pom.scm.url"].toString())
                    }

                    licenses {
                        license {
                            // Usually, the organization contains client's information.
                            name.set(project.extra["pom.organization.name"].toString())
                            url.set(project.extra["pom.license.url"].toString())
                            distribution.set("repo")
                        }
                    }

                    // Usually, the organization contains client's information.
                    organization {
                        name.set(project.extra["pom.organization.name"].toString())
                        url.set(project.extra["pom.organization.url"].toString())
                    }

                    developers {
                        developer {
                            id.set(project.extra["pom.developer.id"].toString())
                            name.set(project.extra["pom.developer.name"].toString())
                            email.set(project.extra["pom.developer.email"].toString())
                        }
                    }
                }
            }
        }

        signing {
            val signingPassword: String? by extra
            val signingKey: String? by extra
            require(signingPassword != null) { "The signingPassword cannot be null." }
            require(signingKey != null) { "The signingKey cannot be null." }
            useInMemoryPgpKeys(base64Decode(signingKey), base64Decode(signingPassword))
            sign(*publishing.publications.toTypedArray())
        }
    }
}




